package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {
	fmt.Println("Max number?")
	scanner := bufio.NewScanner(os.Stdin)

	for c, min, max := 1, 1, getnum(scanner); max > min; c++ {
		bid := (max + min) >> 1
		fmt.Printf("max=%d min=%d bid=%d try=%d\n", max, min, bid, c)

		if i := getnum(scanner); i > 0 { // upper
			fmt.Println("Going upper")
			min = bid + 1
		} else { // lower
			fmt.Println("Going lower")
			max = bid - 1
		}
	}
	fmt.Println("End of loop.")
}

func getnum(scanner *bufio.Scanner) int {
	scanner.Scan()
	i, err := strconv.Atoi(scanner.Text())
	if err != nil {
		log.Fatal(err)
	}
	return i
}
