package main

import (
	"bufio"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strconv"
	"time"
)

func main() {
	fmt.Println("Max number?")
	scanner := bufio.NewScanner(os.Stdin)
	max := getnum(scanner)

	rand.Seed(time.Now().UnixNano())
	r := rand.Intn(max) + 1 // e.g. 1 - 100

	fmt.Printf("Range:1 - %d\n", max)
	fmt.Printf("Minimum number of try:%d\n", due(max))

	for c, i := 1, -1; !(i == 0); c++ { // 0 for exit
		i = getnum(scanner)
		fmt.Println(i)

		if s := map[bool]string{true: "s", false: ""}[c > 1]; i == r {
			fmt.Printf("Bingo! You tried %d time%s.\n", c, s)
			break
		} else if i > r {
			fmt.Println("Too large")
		} else if i < r {
			fmt.Println("Too small")
		}
	}
	fmt.Printf("Answer=%d\n", r) // show answer
}

func getnum(scanner *bufio.Scanner) int {
	scanner.Scan()
	i, err := strconv.Atoi(scanner.Text())
	if err != nil {
		log.Fatal(err)
	}
	return i
}

func due(i int) int {
	c := 1
	if i>>1 > 0 {
		c += due(i >> 1)
	}
	return c
}
